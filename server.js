var express = require('express');
var app = express();
var port = process.env.PORT || 3000;
var morgan = require('morgan');
var mongoose = require('mongoose');
// var autoIncrement = require('mongoose-auto-increment');
var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');

mongoose.Promise = global.Promise;
var connection = mongoose.connect('mongodb://localhost/qawafel', { useMongoClient: true });
// autoIncrement.initialize(connection);

var User = require('./api/models/User');
var Clinic = require('./api/models/Clinic');
var Title = require('./api/models/Title');
var City = require('./api/models/City');
var School = require('./api/models/School');
var Speciality = require('./api/models/Speciality');

var main = require('./api/controllers/MainController');
var users = require('./api/controllers/UserController');
var clinics = require('./api/controllers/ClinicController');
var titles = require('./api/controllers/TitleController');
var cities = require('./api/controllers/CityController');
var schools = require('./api/controllers/SchoolController');
var specialities = require('./api/controllers/SpecialityController');

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({ limit: '50mb' }));

function verifyToken(req, res, next) {
  var token = req.body.token || req.query.token || req.params.token || req.headers['x-access-token'];
  if (token) {
  	jwt.verify(token, 'secret', function(err, decoded) {
  		if (err) {
        return res.json({ success: false, message: 'Authentication failed' });
  		} else {
        req.decoded = decoded;
        next();
  		}
  	});
  } else {
  	return res.json({ success: false, message: 'Authentication failed' });
  }
}

var router = express.Router();

router.get('/seed', main.seedStudents);
router.get('/all', main.all);

router.post('/login', users.login);

router.post('/create-admin', verifyToken, users.createAdmin);
router.post('/update-admin', verifyToken, users.updateAdmin);

router.post('/create-doctor', users.createDoctor);
router.post('/update-doctor', verifyToken, users.updateDoctor);

router.post('/create-student', verifyToken, users.createStudent);
router.get('/read-student', verifyToken, users.readStudent);
router.post('/update-student', verifyToken, users.updateStudent);

router.post('/create-clinic', verifyToken, clinics.create);
router.get('/read-clinics', verifyToken, clinics.read);
router.post('/update-clinic', verifyToken, clinics.update);
router.post('/delete-clinic', verifyToken, clinics.delete);

router.post('/create-title', verifyToken, titles.create);
router.get('/all-titles', titles.all);
router.post('/update-title', verifyToken, titles.update);
router.post('/delete-title', verifyToken, titles.delete);

router.post('/create-city', verifyToken, cities.create);
router.get('/all-cities', cities.all);
router.post('/update-city', verifyToken, cities.update);
router.post('/delete-city', verifyToken, cities.delete);

router.post('/create-school', verifyToken, schools.create);
router.get('/all-schools', schools.all);
router.post('/update-school', verifyToken, schools.update);
router.post('/delete-school', verifyToken, schools.delete);

router.post('/create-speciality', verifyToken, specialities.create);
router.get('/all-specialities', specialities.all);
router.post('/update-speciality', verifyToken, specialities.update);
router.post('/delete-speciality', verifyToken, specialities.delete);

app.use('/api', router);

app.use(function(req, res) {
  return res.status(404).send({ error: req.originalUrl + ' not found' })
});

app.listen(port);

console.log('RESTful API server started on: ' + port);

