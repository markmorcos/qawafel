'use strict';

var mongoose = require('mongoose');
var Speciality = mongoose.model('Speciality');
var jwt = require('jsonwebtoken');

exports.create = function(req, res) {
  var decoded = req.decoded._doc;
  if (decoded.role !== 'admin') {
    res.json({ success: false, message: 'Authentication failed' });
  } else { 
    var speciality = new Speciality({
      name: req.body.name
    });
    speciality.save(function(err, speciality) {
      if (err) return res.send(err);
      res.json(speciality);
    });
  }
};

exports.all = function(req, res) {
  Speciality.find({}, null, { sort: { name: 1 } }, function(err, specialities) {
    if (err) return res.send(err);
    res.json(specialities);
  });
};

exports.update = function(req, res) {
  var decoded = req.decoded._doc;
  if (decoded.role !== 'admin') {
    res.json({ success: false, message: 'Authentication failed' });
  } else {
    Speciality.findById(req.body.id, function(err, speciality) {
      if (err) return res.send(err);
      Speciality.update({ _id: req.body.id }, {
        name: req.body.name
      }, function(err, speciality) {
        if (err) return res.send(err);
        res.json(speciality);
      });
    });
  }
};

exports.delete = function(req, res) {
  var decoded = req.decoded._doc;
  if (decoded.role !== 'admin') {
    res.json({ success: false, message: 'Authentication failed' });
  } else {
    Speciality.findById(req.body.id, function(err, speciality) {
      if (err) return res.send(err);
      Speciality.remove({ _id: req.body.id }, function(err, speciality) {
        if (err) return res.send(err);
        res.json(speciality);
      });
    });
  }
};
