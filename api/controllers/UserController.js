'use strict';

var mongoose = require('mongoose');
var User = mongoose.model('User');
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');

exports.login = function(req, res) {
  User.findOne({ email: req.body.email }, function(err, user) {
    if (err) return res.send(err);
    if (!user) {
      res.json({ success: false, message: 'Authentication failed' });
    } else {
      bcrypt.compare(req.body.password, user.password, function(err, match) {
        if (err) return res.send(err);
        if (!match) {
          res.json({ success: false, message: 'Authentication failed' });
        } else {
          var token = jwt.sign(user, 'secret');
          res.json({ success: true, user: user, token: token });
        }
      });
    }
  });
};

exports.createAdmin = function(req, res) {
  var decoded = req.decoded._doc;
  if (decoded.role !== 'admin') {
    res.json({ success: false, message: 'Authentication failed' });
  } else {
    if (!req.body.email) return res.json({ success: false, message: 'Email is required' });
    if (!req.body.password) return res.json({ success: false, message: 'Password is required' });
    var user = new User({
      role: 'admin',
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      email: req.body.email,
      password: bcrypt.hashSync(req.body.password, 10)
    });
    user.save(function(err, user) {
      if (err) return res.send(err);
      res.json({ success: true, user: user });
    });
  }
};

exports.updateAdmin = function(req, res) {
  var decoded = req.decoded._doc;
  if (decoded._id !== user._id) {
    res.json({ success: false, message: 'Authentication failed' });
  } else {
    User.findById(req.body.id, function(err, user) {
      if (err) return res.send(err);
      if (!user) {
        res.json({ success: false, message: 'Admin not found' });
      } else {
        user.firstName = req.body.firstName || user.firstName;
        user.lastName = req.body.lastName || user.lastName;
        user.email = req.body.email || user.email;
        user.password = req.body.password ? bcrypt.hashSync(req.body.password, 10) : user.password;
        User.update({ _id: req.body.userId }, user, function(err, result) {
          if (err) return res.send(err);
          res.json({ success: true, user: user });
        });
      }
    });
  }
};

exports.createDoctor = function(req, res) {
  if (!req.body.title) return res.json({ success: false, message: 'Title is required' });
  if (!req.body.speciality) return res.json({ success: false, message: 'Speciality is required' });
  if (!req.body.email) return res.json({ success: false, message: 'Email is required' });
  if (!req.body.password) return res.json({ success: false, message: 'Password is required' });
  if (!req.body.mobile) return res.json({ success: false, message: 'Mobile is required' });
  var user = new User({
    role: 'doctor',
    title: req.body.title,
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    speciality: req.body.speciality,
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 10),
    mobile: req.body.mobile
  });
  user.save(function(err, user) {
    if (err) return res.send(err);
    res.json({ success: true, user: user });
  });
};

exports.updateDoctor = function(req, res) {
  var decoded = req.decoded._doc;
  if (decoded.role !== 'admin' && decoded._id !== user._id) {
    res.json({ success: false, message: 'Authentication failed' });
  } else {
    User.findById(req.body.id, function(err, user) {
      if (err) return res.send(err);
      if (!user) {
        res.json({ success: false, message: 'Doctor not found' });
      } else {
        user.firstName = req.body.firstName || user.firstName;
        user.lastName = req.body.lastName || user.lastName;
        user.email = req.body.email || user.email;
        user.password = req.body.password ? bcrypt.hashSync(req.body.password, 10) : user.password;
        User.updateOne({ _id: req.body.id }, user, function(err, result) {
          if (err) return res.send(err);
          res.json({ success: true, user: user });
        });
      }
    });
  }
};

exports.createStudent = function(req, res) {
  if (!req.body.studentId) return res.json({ success: false, message: 'Code is required' });
  if (!req.body.birthdate) return res.json({ success: false, message: 'Birthdate is required' });
  if (!req.body.mobile) return res.json({ success: false, message: 'Mobile is required' });
  if (!req.body.city) return res.json({ success: false, message: 'City is required' });
  if (!req.body.school) return res.json({ success: false, message: 'School is required' });
  var user = new User({
    role: 'student',
    studentId: req.body.studentId,
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    birthdate: req.body.birthdate,
    mobile: req.body.mobile,
    landline: req.body.landline,
    city: req.body.city,
    school: req.body.school,
    knownMedicalConditions: req.body.knownMedicalConditions
  });
  user._id = req.body.id || user._id;
  user.studentId = req.body.sid || user.studentId;
  user.save(function(err, user) {
    if (err) return res.send(err);
    user.populate(['city', 'school'], function(err, user) {
      if (err) return res.send(err);
      res.json({ success: true, user: user });
    });
  });
};

exports.readStudent = function(req, res) {
  if (!req.query.cityId && !req.query.schoolId && !req.query.studentId) {
    return res.json({ success: false, message: 'City, school and student ID are required' });
  }
  User
    .findOne({
      role: 'student',
      city: req.query.cityId,
      school: req.query.schoolId,
      studentId: req.query.studentId
    })
    .populate(['city', 'school'])
    .exec(function(err, user) {
      if (err) return res.send(err);
      if (!user || user.role !== 'student') {
        res.json({ success: false, message: 'Student not found' });
      } else {
        res.json({ success: true, user: user });
      }
    });
};

exports.updateStudent = function(req, res) {
  User.findById(req.body.id, function(err, user) {
    if (err) return res.send(err);
    if (!user) {
      res.json({ success: false, message: 'Student not found' });
    } else {
      user.studentId = req.body.studentId || user.studentId;
      user.firstName = req.body.firstName || user.firstName;
      user.lastName = req.body.lastName || user.lastName;
      user.birthdate = req.body.birthdate || user.birthdate;
      user.mobile = req.body.mobile || user.mobile;
      user.landline = req.body.landline;
      user.city = req.body.city || user.city;
      user.school = req.body.school || user.school;
      user.knownMedicalConditions = req.body.knownMedicalConditions;
      User.updateOne({ _id: req.body.id }, user, function(err, result) {
        if (err) return res.send(err);
        user.populate(['city', 'school'], function(err, user) {
          if (err) res.send(err);
          res.json({ success: true, user: user });
        });
      });
    }
  });
};
