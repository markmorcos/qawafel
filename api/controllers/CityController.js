'use strict';

var mongoose = require('mongoose');
var City = mongoose.model('City');
var jwt = require('jsonwebtoken');

exports.create = function(req, res) {
  var decoded = req.decoded._doc;
  if (decoded.role !== 'admin') {
    res.json({ success: false, message: 'Authentication failed' });
  } else { 
    var city = new City({
      name: req.body.name
    });
    city.save(function(err, city) {
      if (err) return res.send(err);
      res.json(city);
    });
  }
};

exports.all = function(req, res) {
  City.find({}, null, { sort: { name: 1 } }, function(err, cities) {
    if (err) return res.send(err);
    res.json(cities);
  });
};

exports.update = function(req, res) {
  var decoded = req.decoded._doc;
  if (decoded.role !== 'admin') {
    res.json({ success: false, message: 'Authentication failed' });
  } else {
    City.findById(req.body.id, function(err, city) {
      if (err) return res.send(err);
      City.update({ _id: req.body.id }, {
        name: req.body.name
      }, function(err, city) {
        if (err) return res.send(err);
        res.json(city);
      });
    });
  }
};

exports.delete = function(req, res) {
  var decoded = req.decoded._doc;
  if (decoded.role !== 'admin') {
    res.json({ success: false, message: 'Authentication failed' });
  } else {
    City.findById(req.body.id, function(err, city) {
      if (err) return res.send(err);
      City.remove({ _id: req.body.id }, function(err, city) {
        if (err) return res.send(err);
        res.json(city);
      });
    });
  }
};
