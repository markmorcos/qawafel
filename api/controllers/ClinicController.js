'use strict';

var mongoose = require('mongoose');
var User = mongoose.model('User');
var Clinic = mongoose.model('Clinic');
var jwt = require('jsonwebtoken');
var fs = require('fs');

exports.create = function(req, res) {
  var decoded = req.decoded._doc;
  if (decoded.role !== 'doctor' && decoded.role !== 'admin') {
    res.json({ success: false, message: 'Authentication failed' });
  } else {
    var clinic = new Clinic();
    clinic._id = req.body.id || clinic._id;
    const attachments = JSON.parse(req.body.attachments);
    var attachmentUrls = [];
    if (attachments.length) {
      const path = `admin/public/uploads/attachments/${clinic._id}`;
      path.split('/').reduce(function(currentPath, folder) {
        currentPath += folder + '/';
        if (!fs.existsSync(currentPath)) fs.mkdirSync(currentPath);
        return currentPath;
      }, '');
      for (var i = 0; i < attachments.length; ++i) {
        fs.writeFileSync(`${path}/${i}.jpg`, attachments[i].data, 'base64');
        attachmentUrls.push({ caption: attachments[i].caption, data: `/uploads/attachments/${clinic.id}/${i}.jpg` });
      }
    }
    clinic.speciality = req.body.speciality;
    clinic.doctor = req.body.doctor;
    clinic.student = req.body.student;
    clinic.complaint = req.body.complaint;
    clinic.history = req.body.history;
    clinic.findings = req.body.findings;
    clinic.primaryDiagnosis = req.body.primaryDiagnosis;
    clinic.referral = req.body.referral || null;
    clinic.referralReason = clinic.referral ? req.body.referralReason : '';
    clinic.attachments = attachmentUrls;
    clinic.finalDiagnosis = req.body.finalDiagnosis || '';
    clinic.save(function(err, clinic) {
      if (err) return res.send(err);
      res.json({ success: true, clinic: clinic });
    });
  }
};

exports.read = function(req, res) {
  var decoded = req.decoded._doc;
  if (decoded.role !== 'doctor' && decoded.role !== 'admin') {
    res.json({ success: false, message: 'Authentication failed' });
  } else {
    var query = req.query.type === 'primary' ? {
      student: req.query.studentId,
      referral: decoded.speciality
    } : {
      student: req.query.studentId
    };
    Clinic
      .find(query, null, { sort: { createdAt: -1 } })
      .populate(['speciality', 'doctor', 'referralDoctor', 'student', 'referral'])
      .exec(function(err, clinics) {
        if (err) return res.send(err);
        res.json(clinics);
      });
  }
};

const rmdir = function(path) {
  if (fs.existsSync(path)) {
    fs.readdirSync(path).forEach(function(file, index) {
      var curPath = path + "/" + file;
      if (fs.lstatSync(curPath).isDirectory()) {
        deleteFolderRecursive(curPath);
      } else {
        fs.unlinkSync(curPath);
      }
    });
    fs.rmdirSync(path);
  }
};

exports.update = function(req, res) {
  if (!req.body.id) return res.json({ success: false, message: 'ID is required' });
  var decoded = req.decoded._doc;
  if (decoded.role !== 'doctor' && decode.role !== 'admin') {
    res.json({ success: false, message: 'Authentication failed' });
  } else {
    Clinic.findById(req.body.id, function(err, clinic) {
      if (err) return res.send(err);
      if (decoded._id == req.body.doctor) {
        const attachments = JSON.parse(req.body.attachments);
        var attachmentUrls = clinic.attachments;
        if (attachments.length) {
          const path = `admin/public/uploads/attachments/${clinic._id}`;
          path.split('/').reduce(function(currentPath, folder) {
            currentPath += folder + '/';
            if (!fs.existsSync(currentPath)) fs.mkdirSync(currentPath);
            return currentPath;
          }, '');
          const start = clinic.attachments.length;
          const count = clinic.attachments.length + attachments.length;
          for (var i = start; i < count; ++i) {
            fs.writeFileSync(`${path}/${i}.jpg`, attachments[i - clinic.attachments.length].data, 'base64');
            attachmentUrls.push({ caption: attachments[i - clinic.attachments.length].caption, data: `/uploads/attachments/${clinic.id}/${i}.jpg` });
          }
        }
        clinic.finalDiagnosis = req.body.finalDiagnosis || '';
        clinic.attachments = attachmentUrls;
        if (decoded._id == req.body.doctor) {
          clinic.speciality = req.body.speciality || clinic.speciality;
          clinic.doctor = clinic.doctor || clinic.doctor;
          clinic.student = clinic.student || clinic.student;
          clinic.complaint = req.body.complaint || clinic.complaint;
          clinic.history = req.body.history || clinic.history;
          clinic.findings = req.body.findings || clinic.findings;
          clinic.referral = req.body.referral || clinic.referral || null;
          clinic.referralReason = clinic.referral ? req.body.referralReason : '';
          clinic.primaryDiagnosis = req.body.primaryDiagnosis || clinic.primaryDiagnosis;
        }
        Clinic.update({ _id: req.body.id }, clinic, function(err, result) {
          if (err) return res.send(err);
          res.json({ success: true, clinic: clinic });
        });
      } else {
        res.json({ success: false, message: 'Authentication failed' });
      }
    });
  }
};

exports.delete = function(req, res) {
  var decoded = req.decoded._doc;
  if (decoded.role !== 'admin' && decoded.role !== 'doctor') {
    res.json({ success: false, message: 'Authentication failed' });
  } else {
    Clinic.findById(req.body.id, function(err, clinic) {
      if (err) return res.send(err);
      if (decoded._id != clinic.doctor) {
        res.json({ success: false, message: 'Authentication failed' });
      } else {
        const path = `admin/public/uploads/attachments/${clinic._id}`;
        rmdir(path);
        Clinic.remove({ _id: req.body.id }, function(err, result) {
          if (err) return res.send(err);
          res.json({ success: true });
        });
      }
    });
  }
};
