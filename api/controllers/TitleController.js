'use strict';

var mongoose = require('mongoose');
var Title = mongoose.model('Title');
var jwt = require('jsonwebtoken');

exports.create = function(req, res) {
  var decoded = req.decoded._doc;
  if (decoded.role !== 'admin') {
    res.json({ success: false, message: 'Authentication failed' });
  } else { 
    var title = new Title({
      name: req.body.name
    });
    title.save(function(err, title) {
      if (err) return res.send(err);
      res.json(title);
    });
  }
};

exports.all = function(req, res) {
  Title.find({}, null, { sort: { name: 1 } }, function(err, titles) {
    if (err) return res.send(err);
    res.json(titles);
  });
};

exports.update = function(req, res) {
  var decoded = req.decoded._doc;
  if (decoded.role !== 'admin') {
    res.json({ success: false, message: 'Authentication failed' });
  } else {
    Title.findById(req.body.id, function(err, title) {
      if (err) return res.send(err);
      Title.update({ _id: req.body.id }, {
        name: req.body.name
      }, function(err, title) {
        if (err) return res.send(err);
        res.json(title);
      });
    });
  }
};

exports.delete = function(req, res) {
  var decoded = req.decoded._doc;
  if (decoded.role !== 'admin') {
    res.json({ success: false, message: 'Authentication failed' });
  } else {
    Title.findById(req.body.id, function(err, title) {
      if (err) return res.send(err);
      Title.remove({ _id: req.body.id }, function(err, title) {
        if (err) return res.send(err);
        res.json(title);
      });
    });
  }
};
