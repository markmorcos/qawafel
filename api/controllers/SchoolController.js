'use strict';

var mongoose = require('mongoose');
var School = mongoose.model('School');
var jwt = require('jsonwebtoken');

exports.create = function(req, res) {
  var decoded = req.decoded._doc;
  if (decoded.role !== 'admin') {
    res.json({ success: false, message: 'Authentication failed' });
  } else { 
    var school = new School({
      name: req.body.name
    });
    school.save(function(err, school) {
      if (err) return res.send(err);
      res.json(school);
    });
  }
};

exports.all = function(req, res) {
  School.find({}, null, { sort: { name: 1 } }, function(err, schools) {
    if (err) return res.send(err);
    res.json(schools);
  });
};

exports.update = function(req, res) {
  var decoded = req.decoded._doc;
  if (decoded.role !== 'admin') {
    res.json({ success: false, message: 'Authentication failed' });
  } else {
    School.findById(req.body.id, function(err, school) {
      if (err) return res.send(err);
      School.update({ _id: req.body.id }, {
        name: req.body.name
      }, function(err, school) {
        if (err) return res.send(err);
        res.json(school);
      });
    });
  }
};

exports.delete = function(req, res) {
  var decoded = req.decoded._doc;
  if (decoded.role !== 'admin') {
    res.json({ success: false, message: 'Authentication failed' });
  } else {
    School.findById(req.body.id, function(err, school) {
      if (err) return res.send(err);
      School.remove({ _id: req.body.id }, function(err, school) {
        if (err) return res.send(err);
        res.json(school);
      });
    });
  }
};
