'use strict';

var mongoose = require('mongoose');
var City = mongoose.model('City');
var Clinic = mongoose.model('Clinic');
var School = mongoose.model('School');
var Speciality = mongoose.model('Speciality');
var Title = mongoose.model('Title');
var User = mongoose.model('User');
var parseXlsx = require('excel');
var fs = require('fs');


exports.seedStudents = function(req, res) {
  const data = fs.readFileSync('api/seeds/users (copy).csv', 'utf8').split('\n');
  var done = 0;
  var total = data.length - 1;
  var requests = [];
  for (var i = 0; i < total; ++i) {
    const column = data[i].split(',');
    column[0] = column[0].trim();
    var firstName = column[0].split(' ')[0];
    var lastName = column[0].substring(firstName.length + 1);
    var studentId = column[1];
    var birthdate = column[2];
    var mobile = column[3];
    var landline = column[4];
    var city = column[5].trim();
    var school = column[6].trim();
    var knownMedicalConditions = column[7];
    requests.push({
      role: 'student',
      firstName: firstName,
      lastName: lastName,
      studentId: studentId,
      birthdate: birthdate,
      mobile: mobile,
      landline: landline,
      city: city,
      school: school,
      knownMedicalConditions: knownMedicalConditions
    });
  }
  console.log('total: ' + requests.length);
  requests.forEach(function(request, index) {
    const user = new User(request);
    City.findOneOrCreate({ name: request.city }, { name: request.city }, function(err, foundCity) {
      School.findOneOrCreate({ name: request.school }, { name: request.school }, function(err, foundSchool) {
        console.log(foundCity);
        console.log(foundSchool);
        user.city = foundCity;
        user.school = foundSchool;
        user.save(function(err, newUser) {
          if (err) console.log(user.studentId + ': ' + err);
          // else console.log('Saved ' + (index + 1) + ' of ' + total + ' with ID ' + newUser.studentId);
          if (++done === total) return res.json({ success: true });
        });
      });
    });
  });
};

exports.all = function(req, res) {
  City.find({}, null, { sort: { name: 1 } }, function(err, cities) {
    if (err) return res.send(err);
    School.find({}, null, { sort: { name: 1 } }, function(err, schools) {
      if (err) return res.send(err);
      Speciality.find({}, null, { sort: { name: 1 } }, function(err, specialities) {
        if (err) return res.send(err);
        Title.find({}, null, { sort: { name: 1 } }, function(err, titles) {
          if (err) return res.send(err);
          User.find({ role: 'doctor' })
          .populate(['title', 'speciality'])
          .exec(function(err, doctors) {
            if (err) return res.send(err);
            var query = { role: 'student' };
            if (req.query.city) query.city = req.query.city;
            if (req.query.school) query.school = req.query.school;
            User.find(query)
            .populate(['city', 'school'])
            .exec(function(err, students) {
              if (err) return res.send(err);
              Clinic.find({ student: { $in: students.map(function(student) { return student._id }) } })
              .populate(['speciality', 'doctor', 'referralDoctor', 'student', 'referral'])
              .exec(function(err, clinics) {
                if (err) return res.send(err);
                res.json({
                  cities: cities,
                  clinics: clinics,
                  schools: schools,
                  specialities: specialities,
                  titles: titles,
                  doctors: doctors,
                  students: students
                });
              });
            });
          });
        });
      });
    });
  });
};