'use strict';

var mongoose = require('mongoose'), Schema = mongoose.Schema;

var ClinicSchema = new Schema({
  speciality: {
    type: Schema.Types.ObjectId,
    ref: 'Speciality',
    required: 'Speciality is required'
  },
  doctor: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: 'Doctor is required',
    index: true
  },
  student: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: 'Student is required',
    index: true
  },
  complaint: {
    type: String,
    required: 'Complaint is required'
  },
  history: {
    type: String,
    required: 'History is required'
  },
  findings: {
    type: String,
    required: 'Findings is required'
  },
  primaryDiagnosis: {
    type: String,
    required: 'Primary diagnosis is required'
  },
  referral: {
    type: Schema.Types.ObjectId,
    ref: 'Speciality'
  },
  referralDoctor: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    index: true
  },
  referralReason: {
    type: String
  },
  attachments: {
    type: [Schema.Types.Mixed],
    default: []
  },
  finalDiagnosis: {
    type: String
  }
}, { timestamps: true });

module.exports = mongoose.model('Clinic', ClinicSchema);
