'use strict';

var mongoose = require('mongoose'), Schema = mongoose.Schema;
var uniqueValidator = require('mongoose-unique-validator');
// var autoIncrement = require('mongoose-auto-increment');

var UserSchema = new Schema({
  role: {
    type: String,
    enum: ['admin', 'doctor', 'student'],
    default: 'student'
  },
  title: {
    type: Schema.Types.ObjectId,
    ref: 'Title'
  },
  speciality: {
    type: Schema.Types.ObjectId,
    ref: 'Speciality'
  },
  email: {
    type: String,
    unique: 'Email is already taken',
    sparse: true,
    uniqueCaseInsensitive: true
  },
  password: {
    type: String
  },
  studentId: {
    type: String
  },
  firstName: {
    type: String,
    required: 'First name is required'
  },
  lastName: {
    type: String,
    required: 'Last name is required'
  },
  birthdate: {
    type: String,
  },
  mobile: {
    type: String,
  },
  landline: {
    type: String
  },
  city: {
    type: Schema.Types.ObjectId,
    ref: 'City'
  },
  school: {
    type: Schema.Types.ObjectId,
    ref: 'School'
  },
  knownMedicalConditions: {
    type: String
  }
}, { timestamps: true });

UserSchema.plugin(uniqueValidator);
// UserSchema.plugin(autoIncrement.plugin, { model: 'User', field: 'studentId', startAt: 1 });

module.exports = mongoose.model('User', UserSchema);
