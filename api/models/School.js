'use strict';

var mongoose = require('mongoose'), Schema = mongoose.Schema;
var uniqueValidator = require('mongoose-unique-validator');

var SchoolSchema = new Schema({
  name: {
    type: String,
    index: true,
    required: 'Name is required',
    unique: 'School already exists',
    uniqueCaseInsensitive: true
  }
}, { timestamps: true });

SchoolSchema.plugin(uniqueValidator);

SchoolSchema.statics.findOneOrCreate = function findOneOrCreate(condition, doc, callback) {
  const self = this;
  this.findOne(condition, function(err, result) {
    if (result) callback(err, result);
    else self.create(doc, function(newErr, newResult) {
      callback(err, newResult);
    });
  });
};

module.exports = mongoose.model('School', SchoolSchema);
