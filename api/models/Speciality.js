'use strict';

var mongoose = require('mongoose'), Schema = mongoose.Schema;
var uniqueValidator = require('mongoose-unique-validator');

var SpecialitySchema = new Schema({
  name: {
    type: String,
    index: true,
    required: 'Name is required',
    unique: 'Speciality already exists',
    uniqueCaseInsensitive: true
  }
}, { timestamps: true });

SpecialitySchema.plugin(uniqueValidator);

module.exports = mongoose.model('Speciality', SpecialitySchema);
