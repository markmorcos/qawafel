'use strict';

var mongoose = require('mongoose'), Schema = mongoose.Schema;
var uniqueValidator = require('mongoose-unique-validator');

var TitleSchema = new Schema({
  name: {
    type: String,
    index: true,
    required: 'Name is required',
    unique: 'Title already exists',
    uniqueCaseInsensitive: true
  }
}, { timestamps: true });

TitleSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Title', TitleSchema);
