'use strict';

var mongoose = require('mongoose'), Schema = mongoose.Schema;
var uniqueValidator = require('mongoose-unique-validator');

var CitySchema = new Schema({
  name: {
    type: String,
    index: true,
    required: 'Name is required',
    unique: 'City already exists',
    uniqueCaseInsensitive: true
  }
}, { timestamps: true });

CitySchema.plugin(uniqueValidator);

CitySchema.statics.findOneOrCreate = function findOneOrCreate(condition, doc, callback) {
  const self = this;
  this.findOne(condition, function(err, result) {
    if (result) callback(err, result);
    else self.create(doc, function(newErr, newResult) {
      callback(err, newResult);
    });
  });
};

module.exports = mongoose.model('City', CitySchema);
