var keystone = require('keystone');
var Types = keystone.Field.Types;
// var autoIncrement = require('mongoose-auto-increment');

/**
 * User Model
 * ==========
 */
var User = new keystone.List('User');

User.add({
  studentId: { type: String, dependsOn: { role: 'student' } },
  title: { type: Types.Relationship, ref: 'Title', initial: true, dependsOn: { role: 'doctor' } },
  firstName: { type: String, initial: true, required: true },
  lastName: { type: String, initial: true, required: true },
  speciality: { type: Types.Relationship, ref: 'Speciality', initial: true, dependsOn: { role: 'doctor' } },
  email: { type: Types.Email, initial: true, unique: true, sparse: true, dependsOn: { role: ['admin', 'doctor'] }, required: true },
  password: { type: Types.Password, initial: true, dependsOn: { role: ['admin', 'doctor'] }, required: true },
  birthdate: { type: String, initial: true, dependsOn: { role: 'student' }, required: true },
  mobile: { type: String, initial: true, dependsOn: { role: ['doctor', 'student'] }, required: true },
  landline: { type: String, initial: true, dependsOn: { role: 'student' } },
  city: { type: Types.Relationship, ref: 'City', initial: true, dependsOn: { role: 'student' } },
  school: { type: Types.Relationship, ref: 'School', initial: true, dependsOn: { role: 'student' } },
  knownMedicalConditions: { type: String, initial: true, dependsOn: { role: 'student' } },
  role: { type: Types.Select, initial: true, required: true, noedit: true, options: 'admin, doctor, student', default: 'doctor' }
});

// Provide access to Keystone
User.schema.virtual('canAccessKeystone').get(function () {
	return this.role === 'admin';
});

// autoIncrement.initialize(keystone.mongoose.connection);
// User.schema.plugin(autoIncrement.plugin, { model: 'User', field: 'studentId' });

/**
 * Registration
 */
User.defaultColumns = 'studentId, firstName, lastName, city, school, role';
User.register();
