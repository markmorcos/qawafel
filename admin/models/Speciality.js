var keystone = require('keystone');

/**
 * Speciality Model
 * =============
 */

var Speciality = new keystone.List('Speciality');

Speciality.add({
  name: { type: String, required: true }
});

Speciality.defaultSort = 'name';
Speciality.defaultColumns = 'name';

Speciality.register();
