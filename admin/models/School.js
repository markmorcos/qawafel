var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * School Model
 * =============
 */

var School = new keystone.List('School');

School.add({
  name: { type: String, required: true },
  city: { type: Types.Relationship, ref: 'City' }  
});

School.defaultSort = 'name';
School.defaultColumns = 'name';

School.register();
