var keystone = require('keystone');

/**
 * City Model
 * =============
 */

var City = new keystone.List('City');

City.add({
  name: { type: String, required: true }
});

City.defaultSort = 'name';
City.defaultColumns = 'name';

City.register();
