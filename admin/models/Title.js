var keystone = require('keystone');

/**
 * Title Model
 * =============
 */

var Title = new keystone.List('Title');

Title.add({
  name: { type: String, required: true }
});

Title.defaultSort = 'name';
Title.defaultColumns = 'name';

Title.register();
