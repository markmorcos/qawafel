var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Clinic Model
 * =============
 */

var Clinic = new keystone.List('Clinic', {
  nocreate: true,
  noedit: true
});

Clinic.add({
  speciality: { type: Types.Relationship, ref: 'Speciality' },
  doctor: { type: Types.Relationship, ref: 'User' },
  student: { type: Types.Relationship, ref: 'User' },
  complaint: { type: String },
  history: { type: String },
  findings: { type: String },
  primaryDiagnosis: { type: String },
  referral: { type: Types.Relationship, ref: 'Speciality' },
  referralDoctor: { type: Types.Relationship, ref: 'User' },
  referralReason: { type: String },
  attachments: { type: Types.TextArray, format: false },
  finalDiagnosis: { type: String },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now }
});

Clinic.defaultSort = '~createdAt';
Clinic.defaultColumns = 'speciality, doctor, createdAt';

Clinic.register();
